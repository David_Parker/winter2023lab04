import java.util.Scanner;
public class NationalPark {
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Dolphin[] pod = new Dolphin[4];
		
		//Creating pod array objects
		for(int i=0; i<pod.length; i++) {
			System.out.println("What is the weight of dolphin #" + (i+1) + " in kg?");
			double weight = reader.nextDouble();
			System.out.println("What is the swimming speed of dolphin #" + (i+1) + " in km/h?");
			int swimmingSpeed = reader.nextInt();
			System.out.println("Does dolphin #" + (i+1) + " have a long snout?(true or false)");
			boolean longSnout = reader.nextBoolean();
			pod[i] = new Dolphin(swimmingSpeed, weight, longSnout);
			System.out.println("");
		}
		
		//Printing last dolphin description before setting new values
		System.out.println("This is the description of the last dolphin:");
		System.out.println("The dolphin weighs: " + pod[pod.length-1].getWeight() + "kg");
		System.out.println("The dolphin swims at a speed of: " + pod[pod.length-1].getSwimmingSpeed() + "km/h");
		System.out.println("Does the dolphin have a long snout? : " + pod[pod.length-1].getLongSnout());
		System.out.println("");
		
		//Updating fields for last dolphin
		System.out.println("Please update the values for the last dolphin:");
		System.out.println("Please change the dolphin's weight:");
		double lastDolphinWeight = reader.nextDouble();
		System.out.println("Please change the dolphin's swimming speed:");
		int lastDolphinSwimmingSpeed = reader.nextInt();
		pod[pod.length-1].setWeight(lastDolphinWeight);
		pod[pod.length-1].setSwimmingSpeed(lastDolphinSwimmingSpeed);
		
		System.out.println("");
		
		//Last dolphin description
		System.out.println("This is the new description of the last dolphin:");
		System.out.println("The dolphin weighs: " + pod[pod.length-1].getWeight() + "kg");
		System.out.println("The dolphin swims at a speed of: " + pod[pod.length-1].getSwimmingSpeed() + "km/h");
		System.out.println("Does the dolphin have a long snout? : " + pod[pod.length-1].getLongSnout());
		
		System.out.println("");
		
		//First dolphin actions
		System.out.println("The first dolphin performed these actions:");
		pod[0].jumpOutOfWater();
		int food1 = pod[0].catchFood();
		
		System.out.println("");
		
		//Using eatFood method on 2nd dolphin
		System.out.println("The second dolphin performed these actions:");
		int food2 = pod[1].catchFood();
		pod[1].eatFood(food2);
		System.out.println("The dolphin's new weight is: " + pod[1].getWeight() + " kg!");
	}
}