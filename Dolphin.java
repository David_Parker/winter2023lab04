import java.util.Random;
public class Dolphin {
	//Fields
	private int swimmingSpeed;
	private double weight;
	private boolean longSnout;
	
	//Constructor
	public Dolphin(int swimmingSpeed, double weight, boolean longSnout) {
		this.swimmingSpeed = swimmingSpeed;
		this.weight = weight;
		this.longSnout = longSnout;
	}
	
	//Get methods
	public int getSwimmingSpeed() {
		return this.swimmingSpeed;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public boolean getLongSnout() {
		return this.longSnout;
	}
	
	//Set methods
	public void setSwimmingSpeed(int swimmingSpeed) {
		this.swimmingSpeed = swimmingSpeed;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	//Custom Instance Methods
	public void jumpOutOfWater() {
		double jumpHeight = this.swimmingSpeed - (this.weight*0.05);
		System.out.println("The dolphin jumped " + jumpHeight + "cm into the air!");
	}
	
	public int catchFood() {
		Random rand = new Random();
		if(this.longSnout) {
			System.out.println("The dolphin caught a crab!");
		}
		else {
		System.out.println("The dolphin caught a fish!");
		}
		int foodAmount = rand.nextInt(41);
		return foodAmount;
	}
	
	public void eatFood(int foodAmount) {
		Random rand = new Random();
		int gainedWeight = 0;
		eatFoodValid(foodAmount);
		if(foodAmount >= 0 && foodAmount <= 10) {
			gainedWeight = rand.nextInt(5)+1;
			this.weight -= gainedWeight;
			System.out.println("The dolphin remains hungry and loses " + gainedWeight + " kg!");
		}
		else if(foodAmount > 10 && foodAmount <= 20) {
			gainedWeight = rand.nextInt(5)+1;
			this.weight += gainedWeight;
			System.out.println("The dolphin ate normally and has gained " + gainedWeight + " kg!");
		}
		else if(foodAmount > 20 && foodAmount <=30) {
			gainedWeight = rand.nextInt(6)+5;
			this.weight += gainedWeight;
			System.out.println("The dolphin had a delicious meal and gained " + gainedWeight + " kg!");
		}
		else if(foodAmount >30 && foodAmount <=40) {
			gainedWeight = rand.nextInt(foodAmount)+30;
			this.weight += gainedWeight;
			System.out.println("The dolphin had a buffet with its friends and gained " + gainedWeight + " kg!");
		}
	}
	
	//Helper validator method for eatFood
	private static void eatFoodValid(int foodAmount) {
		if(foodAmount < 0) {
			System.out.println("Invalid input");
		}
		else if(foodAmount > 40) {
			System.out.println("Invalid input");
		}
	}
}